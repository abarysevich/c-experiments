//============================================================================
// Name        : SetTest.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <set>

using namespace std;

int main() {
	set<int> test_set;

	// Element is absent
	cout << (test_set.find(5) == test_set.end()) << "\n";

	test_set.insert(5);
	// Element exists
	cout << (test_set.find(5) == test_set.end()) << "\n";
	return 0;
}
