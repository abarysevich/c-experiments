/*
 * ReferenceTypeCallsTest.cpp
 *
 *  Created on: Oct 26, 2014
 *      Author: abarysevich
 */

#include <iostream>

struct Struct {
	Struct (std::string name) {
		struct_name = name;
		a = 0;
		b = 0;
	}

	std::string struct_name;
	int a;
	int b;
};

void printStruct(Struct& struc) {
	std::cout << struc.struct_name << " value: "<< struc.a << "\n";
}

int main() {
	Struct firstStruct("first struct");
	firstStruct.a = 1;
	// Copy is created
	Struct secondStruct = firstStruct;
	secondStruct.struct_name = "second struct";
	printStruct(firstStruct);
	// Copy doesn't have the recent changes to the struct
	printStruct(secondStruct);

	// Additional reference to the object is created
	Struct& thirdStruct = firstStruct;
	firstStruct.a = 3;
	printStruct(firstStruct);
	// Additional reference has the recent changes to the struct
	printStruct(thirdStruct);

	// replaces currently referenced firstStruct object with secondStruct object copy;
	// as a result firstStruct object is lost
	thirdStruct = secondStruct;

	printStruct(thirdStruct);
	printStruct(firstStruct);
	printStruct(secondStruct);

	firstStruct.a = 5;
	printStruct(firstStruct);
	printStruct(secondStruct);
	return 0;
}
