//============================================================================
// Name        : FriendTest.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

namespace test{

class ArrayWrapper {
 public:
	ArrayWrapper() {
		arr[0] = 1;
		arr[1] = 1;
		arr[2] = 1;
		arr[3] = 1;
	}
	friend void Print(const ArrayWrapper array_wrapper);
 private:
	int arr [4];
};

void Print(const ArrayWrapper array_wrapper){
		cout << array_wrapper.arr[1];
	}
}

int main() {
	test::ArrayWrapper wrapper;
	test::Print(wrapper);
	return 0;
}

