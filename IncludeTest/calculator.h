/*
 * calculator.h
 *
 *  Created on: Nov 4, 2014
 *      Author: abarysevich
 */

#ifndef CALCULATOR_H_
#define CALCULATOR_H_

namespace calculator{

class Calculator {
public:
	int add(const int& first, const int& second);
};
}

#endif /* CALCULATOR_H_ */
