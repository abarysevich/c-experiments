//============================================================================
// Name        : VirtualFunctionTest.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <stdio.h>

using namespace std;

namespace test{
	class Superclass {
		public:
			virtual void VirtualFunction() = 0;

			void NonVirtualFunction();
	};

	class Subclass : public Superclass {
		public:
			virtual void VirtualFunction();

			void NonVirtualFunction();
	};

};

// Despite the function existing implementation - if it is declared as an
// abstract (' = 0' postfix ) entire class is considered as an abstract
// class - such class instantiation is prohibited!
void ::test::Superclass::VirtualFunction() {
	printf("\n Superclass virtual function call!! \n");
}

void ::test::Superclass::NonVirtualFunction() {
	printf("\n Superclass not virtual function call!! \n");
}

void ::test::Subclass::VirtualFunction() {
	printf("\n Subclass virtual function call!! \n");
}

void ::test::Subclass::NonVirtualFunction() {
	printf("\n Subclass virtual function call!! \n");
}

void print(::test::Superclass& instance) {
	instance.NonVirtualFunction();
	instance.VirtualFunction();
}

int main() {
//	Can't be instantiated since it's an abstract class
//	::test::Superclass superclass;
//	print(superclass);

	::test::Subclass subclass;
	subclass.NonVirtualFunction();

	print(subclass);
	return 0;
}
