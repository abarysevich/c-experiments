//============================================================================
// Name        : StackOverflowTest.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
#include <stdio.h>

using namespace std;

struct A { int weight[1000000000000];};

int main() {
	vector<A> vect;

	printf("Hello!");

	int iteration = 0;
	while (true) {
		A array;
		vect.push_back(array);
		printf("Current iteration is: %d \n", iteration++);
	}

	return 0;
}
