/*
 * launcher.cc
 *
 *  Created on: Apr 25, 2015
 *      Author: abarysevich
 */

#include "implemented_pure_virtual_function.h"
#include "unimplemented_function.h"
#include "pure_virtual_function.h"

int main() {
	::test::NonAbstractClass non_abstract;
	non_abstract.NonImplementedFunction();

	::test::NonAbstractSubclass not_abstract;
	not_abstract.VirtualFunction();
	not_abstract.NonVirtualFunction();
	return 0;
}

