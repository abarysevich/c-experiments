/*
 * implemented_pure_virtual_function.h
 *
 *  Created on: Apr 25, 2015
 *      Author: abarysevich
 */

#ifndef IMPLEMENTED_PURE_VIRTUAL_FUNCTION_H_
#define IMPLEMENTED_PURE_VIRTUAL_FUNCTION_H_

#include "pure_virtual_function.h"

namespace test {

class NonAbstractSubclass : public AbstractClass {
public:
	void VirtualFunction();
};
}

#endif /* IMPLEMENTED_PURE_VIRTUAL_FUNCTION_H_ */
