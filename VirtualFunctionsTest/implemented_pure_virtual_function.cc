/*
 * implemented_pure_virtual_function.cc
 *
 *  Created on: Apr 25, 2015
 *      Author: abarysevich
 */
#include <iostream>

#include "implemented_pure_virtual_function.h"

namespace test {
void NonAbstractSubclass::VirtualFunction() {
	::std::cout << "Pure virtual function implementation in subclass \n";
}
}
