/*
 * unimplemented_virtual_function.cc
 *
 *  Created on: Apr 25, 2015
 *      Author: abarysevich
 */

#include "unimplemented_function.h"

#include <iostream>

namespace test{
NonAbstractClass::NonAbstractClass(){};

NonAbstractClass::~NonAbstractClass(){};

void NonAbstractClass::ImplementedFunction() {
	::std::cout << "Implemented function call! \n";
}

void NonAbstractClass::NonImplementedFunction() {
	::std::cout << "Non implemented function call! \n";
}
}
