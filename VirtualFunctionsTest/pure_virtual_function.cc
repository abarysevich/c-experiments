/*
 * unimplemented_pure_virtual_function.cc
 *
 *  Created on: Apr 25, 2015
 *      Author: abarysevich
 */
#include "pure_virtual_function.h"

#include <iostream>

namespace test{

AbstractClass::AbstractClass(){};
AbstractClass::~AbstractClass(){};

void AbstractClass::NonVirtualFunction(){
	::std::cout << "Non virtual function call! \n";
}

void AbstractClass::VirtualFunction(){
	::std::cout << "Pure virtual function call! \n";
}
}


