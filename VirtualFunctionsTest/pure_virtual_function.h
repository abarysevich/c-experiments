/*
 * unimplemented_pure_virtual_function.h
 *
 *  Created on: Apr 25, 2015
 *      Author: abarysevich
 */

#ifndef UNIMPLEMENTED_PURE_VIRTUAL_FUNCTION_H_
#define UNIMPLEMENTED_PURE_VIRTUAL_FUNCTION_H_

namespace test{
	class AbstractClass {
	public:
		AbstractClass();
		virtual ~AbstractClass();

		// Class that contains at least one pure virtual function is considered
		// an abstract and can't be instantiated even if that function was
		// implemented in .cc file for that class
		virtual void VirtualFunction() = 0;

		void NonVirtualFunction();
	};
}
#endif /* UNIMPLEMENTED_PURE_VIRTUAL_FUNCTION_H_ */
