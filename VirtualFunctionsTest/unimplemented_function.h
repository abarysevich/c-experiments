/*
 * unimplemented_virtual_function.h
 *
 *  Created on: Apr 25, 2015
 *      Author: abarysevich
 */

#ifndef UNIMPLEMENTED_FUNCTION_H_
#define UNIMPLEMENTED_FUNCTION_H_

namespace test {
	class NonAbstractClass {
	public:
		NonAbstractClass();
		virtual ~NonAbstractClass();

		// Compiles successfully, however linking fails with
		// undefined reference to `test::NonAbstractClass::VirtualFunction()
		// error!!!
		virtual void NonImplementedFunction();

		void ImplementedFunction();
	};
}

#endif /* UNIMPLEMENTED_FUNCTION_H_ */
