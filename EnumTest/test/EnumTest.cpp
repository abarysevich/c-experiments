/*
 * EnumTest.cpp
 *
 *  Created on: Oct 26, 2014
 *      Author: abarysevich
 */

# include <iostream>

namespace test {
enum Test{FIRST, SECOND, THIRD};
}

int main() {
	test::Test a = test::FIRST;
	std::cout << a << "\n";

	test::Test b =  test::Test(2);
	std::cout << b << "\n";

	test::Test c =  test::Test(99);
	std::cout << c << "\n";

}
