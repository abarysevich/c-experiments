/*
 * function_b.cc
 *
 *  Created on: Apr 19, 2015
 *      Author: abarysevich
 */
#include <stdio.h>

#include "function_b.h"

namespace function {
	void functionB() {
		printf("Hello from B");
	}
}
