/*
 * function_a.cc
 *
 *  Created on: Apr 19, 2015
 *      Author: abarysevich
 */
#include <stdio.h>
#include "function_b.h"

void functionA() {
	printf("Hello from A");
}

//extern void functionB();
//
//void functionB() {
//	printf("Hello from B");
//}


int main() {
	functionA();
	printf("\n");
	::function::functionB();
}

