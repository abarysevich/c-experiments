/*
 * function_b.h
 *
 *  Created on: Apr 19, 2015
 *      Author: abarysevich
 */

#ifndef FUNCTION_B_H_
#define FUNCTION_B_H_


namespace function {
	void functionB();
}


#endif /* FUNCTION_B_H_ */
