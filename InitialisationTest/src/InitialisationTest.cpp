//============================================================================
// Name        : InitialisationTest.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>

using namespace std;

class ImplicitDefaultConstructor {
public:
	int primitive_value;
	vector<string> reference_type_object;
};

class ExplicitDefaultConstructor {
public:
	ExplicitDefaultConstructor() {
	}

	int primitive_value;
	vector<string> reference_type_object;
};

class ExplicitConstructor {
public:
	ExplicitConstructor() {
		primitive_value = 3;
	}

	int primitive_value;
	vector<string> reference_type_object;
};

int main() {
	ImplicitDefaultConstructor implicit_default_constructor;
	cout << "implicit_default_constructor primitive_value: "
		 <<  implicit_default_constructor.primitive_value
		 << "\n";

	implicit_default_constructor.reference_type_object.push_back("string");
	cout << "implicit_default_constructor reference_type_object size: "
		 <<  implicit_default_constructor.reference_type_object.size()
		 << "\n";

	ExplicitDefaultConstructor explicit_default_constructor;
	cout << "explicit_default_constructor primitive_value: "
		 <<  explicit_default_constructor.primitive_value
		 << "\n";

	explicit_default_constructor.reference_type_object.push_back("string");
	cout << "explicit_default_constructor reference_type_object size: "
		 <<  explicit_default_constructor.reference_type_object.size()
		 << "\n";

	ExplicitConstructor explicit_constructor;
	cout << "explicit_constructor primitive_value: "
		 <<  explicit_constructor.primitive_value
		 << "\n";

	explicit_constructor.reference_type_object.push_back("string");
	cout << "explicit_constructor reference_type_object size: "
		 <<  explicit_constructor.reference_type_object.size()
		 << "\n";

	return 0;
}
