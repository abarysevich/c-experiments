/*
 * Test.cpp
 *
 *  Created on: Oct 20, 2014
 *      Author: abarysevich
 */

#include <iostream>

void PrintVariables(int& a, int& b) {
	std::cout << "\n a = " << a << " b = " << b << "\n";
}

void IncrementValues(int& a, int& b) {
	a++;
	b++;
	std::cout << " inside the increment method ";
	PrintVariables(a, b);
}

void IncrementValuesByValue(int a, int b) {
	a++;
	b++;
	std::cout << " inside the increment method ";
	PrintVariables(a, b);
}

int main() {
	int a = 1;
	int b = 2;
	std::cout << " before ";
	PrintVariables(a, b);
	IncrementValues(a, b);
	std::cout << " after ";
	PrintVariables(a, b);
	IncrementValuesByValue(a, b);
	std::cout << " after ";
	PrintVariables(a, b);
	return 0;
}



