/*
 * PrimitiveTypesCast.cpp
 *
 *  Created on: Oct 26, 2014
 *      Author: abarysevich
 */

# include <limits>
# include <iostream>

using namespace std;

int main() {
	long int_max = numeric_limits<int>::max();
	// No compile time error!!! - int_value value is rubbish!
	int int_value = int_max + 1;
	cout << int_value;
}
